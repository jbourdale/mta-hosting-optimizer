export default class ConfigurationService {
    // Process
    static environment = process.env.NODE_ENV;
    static logLevel = process.env.LOG_LEVEL || "info";
    static isDevelopment = ConfigurationService.environment === "development";

    // HTTP
    static host: string = process.env.HOST || "127.0.0.1";
    static port: number = parseInt(process.env.PORT, 10) || 3000;

    // IP Config Service
    static ipConfigProtocol: string = process.env.IP_CONFIG_PROTOCOL || "http";
    static ipConfigHost: string = process.env.IP_CONFIG_HOST || "ipconfig";
    static ipConfigPort: number =
        parseInt(process.env.IP_CONFIG_PORT, 10) || 8080;
    static ipConfigTimeout: number =
        parseInt(process.env.IP_CONFIG_TIMEOUT, 10) || 2000;
    static ipConfigThreshold: number =
        parseInt(process.env.IP_CONFIG_THRESHOLD, 10) || 1;
}
