import axios from "axios";

import ConfigurationService from "./Configuration";

export type IpConfigUsage = {
    hostname: string;
    usage: number;
};

type IpConfig = {
    ip: string;
    hostname: string;
    active: boolean;
};

export default class IpConfigService {
    timeout: number;
    baseUrl: string;
    endpoint: string;

    constructor() {
        const protocol = ConfigurationService.ipConfigProtocol;
        const host = ConfigurationService.ipConfigHost;
        const port = ConfigurationService.ipConfigPort;

        this.timeout = ConfigurationService.ipConfigTimeout;
        this.endpoint = "ip-configs";
        this.baseUrl = `${protocol}://${host}:${port}/${this.endpoint}`;
    }

    async getIpsUsage(): Promise<IpConfigUsage[]> {
        const rawIpConfigs = await this._fetch();

        return rawIpConfigs.reduce((acc, ip) => {
            let ipUsage = acc.find((e) => e.hostname === ip.hostname);
            if (!ipUsage) {
                ipUsage = { hostname: ip.hostname, usage: 0 };
                acc.push(ipUsage);
            }

            if (ip.active) {
                ipUsage.usage++;
            }

            return acc;
        }, []);
    }

    async _fetch(): Promise<IpConfig[]> {
        try {
            const response = await axios.get(this.baseUrl, {
                timeout: this.timeout,
            });

            return response.data as IpConfig[];
        } catch (err) {
            throw "Unable to reach IpConfigService";
        }
    }
}
