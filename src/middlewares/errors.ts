import express from "express";

import { HttpError, NotFoundHttpError } from "../errors";

export const notFoundErrorHandler = (req, res, next) => {
    next(new NotFoundHttpError());
};

export const devErrorHandler = (
    err: HttpError,
    req: express.Request,
    res: express.Response,
    next: () => any
) => {
    console.error(err.stack); // TODO : update with proper logger

    productionErrorHandler(err, req, res, next);
};

export const productionErrorHandler = (
    err: HttpError,
    req: express.Request,
    res: express.Response,
    next: () => any
) => {
    res.status(err.statusCode || 500);
    res.json(buildErrorJsonResponse(err));
};

const buildErrorJsonResponse = (error: HttpError) => ({
    error: {
        message: error.message,
        statusCode: error.statusCode,
    },
});
