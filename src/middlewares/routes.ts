import express from "express";

import { threshold } from "../api";

const router = express.Router();
router.use("/threshold", threshold);

export default router;
