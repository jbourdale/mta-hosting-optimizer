export class HttpError extends Error {
    statusCode: number;

    constructor(statusCode: number, message?: string) {
        super(message);
        this.statusCode = statusCode;
    }
}

export class NotFoundHttpError extends HttpError {
    constructor() {
        super(404, "Not Found");
    }
}

export class IpConfigServiceError extends HttpError {
    constructor() {
        super(400, "Unable to retrieve IpConfig data");
    }
}
