import { IpConfigService } from "../services";
import { IpConfigServiceError } from "../errors";
import { ConfigurationService } from "../services";

export default async (req, res, next) => {
    let ipConfigs;
    try {
        ipConfigs = await new IpConfigService().getIpsUsage();
    } catch {
        next(new IpConfigServiceError());
        return;
    }

    const unsufisantUsedIpConfigs = ipConfigs.filter(
        (ipConfig) => ipConfig.usage <= ConfigurationService.ipConfigThreshold
    );

    res.send({
        hostnames: unsufisantUsedIpConfigs.map((ipConfig) => ipConfig.hostname),
    });
};
