import Server from "./Server";

import { ConfigurationService } from "./services";

new Server()
  .init()
  .run()
  .then(() => {
    console.log(
      `Server running on ${ConfigurationService.host}:${ConfigurationService.port}`
    );
  })
  .catch((error) => {
    console.error("An error occured while running the server : ", error);
  });
