import express from "express";
import cors from "cors";
import morgan from "morgan";

import { ConfigurationService } from "./services";
import {
    routes,
    notFoundErrorHandler,
    devErrorHandler,
    productionErrorHandler,
} from "./middlewares";

export default class Server {
    app: express.Application;

    constructor() {
        this.app = null;
    }

    init(): Server {
        this.app = express();

        this.app.use(cors());

        // Only accept and parse JSON
        this.app.use(express.json());

        this.app.use(morgan("tiny"));

        this.app.use(routes);

        // catch 404 and forward to error handler
        this.app.use(notFoundErrorHandler);

        // Error handlers
        if (ConfigurationService.isDevelopment) {
            this.app.use(devErrorHandler);
        }
        this.app.use(productionErrorHandler);

        return this;
    }

    async run(): Promise<any> {
        return new Promise((resolve, reject) => {
            const instance = this.app.listen(
                ConfigurationService.port,
                ConfigurationService.host,
                () => {
                    resolve(instance);
                }
            );

            instance.on("error", (error) => {
                reject(error);
            });
        });
    }
}
