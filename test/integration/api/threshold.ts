import request from "supertest";
import axios from "axios";
import { mocked } from "ts-jest/utils";

import Server from "../../../src/Server";

jest.mock("axios");

describe("Threshold", () => {
    it("GET /threshold must return an array of hostname bellow ipConfigThreshold", (done) => {
        const app = new Server().init().app;

        mocked(axios.get).mockResolvedValue({
            data: [
                { ip: "127.0.0.1", hostname: "mta-prod-1", active: true },
                { ip: "127.0.0.2", hostname: "mta-prod-1", active: false },
                { ip: "127.0.0.3", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.4", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.5", hostname: "mta-prod-2", active: false },
                { ip: "127.0.0.6", hostname: "mta-prod-3", active: false },
            ],
        });

        request(app)
            .get("/threshold")
            .then((response) => {
                expect(response.statusCode).toEqual(200);
                expect(response.body).toEqual({
                    hostnames: ["mta-prod-1", "mta-prod-3"],
                });
                done();
            });
    });

    it("GET /threshold must return a 400 error when IpConfigService raise an error", (done) => {
        const app = new Server().init().app;

        mocked(axios.get).mockRejectedValue("Error");

        request(app)
            .get("/threshold")
            .then((response) => {
                expect(response.statusCode).toEqual(400);
                expect(response.body).toEqual({
                    error: {
                        message: "Unable to retrieve IpConfig data",
                        statusCode: 400,
                    },
                });

                done();
            });
    });

    it("GET /threshold must return an empty array when no hostname is bellow the ipConfigTreshold", (done) => {
        const app = new Server().init().app;

        mocked(axios.get).mockResolvedValue({
            data: [
                { ip: "127.0.0.1", hostname: "mta-prod-1", active: true },
                { ip: "127.0.0.2", hostname: "mta-prod-1", active: true },
                { ip: "127.0.0.3", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.4", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.5", hostname: "mta-prod-3", active: true },
                { ip: "127.0.0.6", hostname: "mta-prod-3", active: true },
            ],
        });

        request(app)
            .get("/threshold")
            .then((response) => {
                expect(response.statusCode).toEqual(200);
                expect(response.body).toEqual({
                    hostnames: [],
                });

                done();
            });
    });
});
