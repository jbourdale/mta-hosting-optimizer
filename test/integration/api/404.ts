import request from "supertest";

import Server from "../../../src/Server";

describe("404", () => {
    it("GET / must return a 404", (done) => {
        const app = new Server().init().app;

        request(app)
            .get("/")
            .then((response) => {
                expect(response.statusCode).toEqual(404);
                expect(response.body).toEqual({
                    error: { message: "Not Found", statusCode: 404 },
                });
                done();
            });
    });
});
