describe("ConfigurationService", () => {
    beforeEach(() => {
        process.env.NODE_ENV = "test";
        process.env.LOG_LEVEL = "logLevel";
        process.env.HOST = "host";
        process.env.PORT = "1234";

        process.env.IP_CONFIG_PROTOCOL = "protocol";
        process.env.IP_CONFIG_HOST = "ipConfigHost";
        process.env.IP_CONFIG_PORT = "5678";
        process.env.IP_CONFIG_TIMEOUT = "9999";
        process.env.IP_CONFIG_THRESHOLD = "2";
        jest.resetModules();

        this.service = require("../../../src/services/Configuration").default;
    });

    test("it should load environment config", () => {
        expect(this.service.environment).toBe(process.env.NODE_ENV);
    });

    test("it should set isDevelopment to true", () => {
        process.env.NODE_ENV = "development";
        jest.resetModules();
        this.service = require("../../../src/services/Configuration").default;

        expect(this.service.isDevelopment).toBe(true);
    });

    test("it should set isDevelopment to false", () => {
        process.env.NODE_ENV = "production";
        jest.resetModules();
        this.service = require("../../../src/services/Configuration").default;

        expect(this.service.isDevelopment).toBe(false);
    });

    test("it should load loglevel config", () => {
        expect(this.service.logLevel).toBe(process.env.LOG_LEVEL);
    });

    test("it should load host config", () => {
        expect(this.service.host).toBe(process.env.HOST);
    });

    test("it should load port config", () => {
        expect(this.service.port).toBe(parseInt(process.env.PORT, 10));
    });

    describe("IpConfig WebService Configuration", () => {
        test("it should load protocol config", () => {
            expect(this.service.ipConfigProtocol).toBe(
                process.env.IP_CONFIG_PROTOCOL
            );
        });

        test("it should load host config", () => {
            expect(this.service.ipConfigHost).toBe(process.env.IP_CONFIG_HOST);
        });

        test("it should load port config", () => {
            expect(this.service.ipConfigPort).toBe(
                parseInt(process.env.IP_CONFIG_PORT, 10)
            );
        });

        test("it should load timeout config", () => {
            expect(this.service.ipConfigTimeout).toBe(
                parseInt(process.env.IP_CONFIG_TIMEOUT, 10)
            );
        });

        test("it should load threshold config", () => {
            expect(this.service.ipConfigThreshold).toBe(
                parseInt(process.env.IP_CONFIG_THRESHOLD, 10)
            );
        });
    });
});
