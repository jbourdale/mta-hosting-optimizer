import { mocked } from "ts-jest/utils";

import IpConfigService, { IpConfigUsage } from "../../../src/services/IpConfig";

describe("IpConfig Service", () => {
    describe("getIpsUsage", () => {
        test("it should aggregate the number of active Ip per host", (done) => {
            const service = new IpConfigService();

            jest.spyOn(service, "_fetch");
            mocked(service._fetch).mockResolvedValue([
                { ip: "127.0.0.1", hostname: "mta-prod-1", active: true },
                { ip: "127.0.0.2", hostname: "mta-prod-1", active: false },
                { ip: "127.0.0.3", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.4", hostname: "mta-prod-2", active: true },
                { ip: "127.0.0.5", hostname: "mta-prod-2", active: false },
                { ip: "127.0.0.6", hostname: "mta-prod-3", active: false },
            ]);

            service.getIpsUsage().then((result) => {
                expect(result).toEqual([
                    { hostname: "mta-prod-1", usage: 1 },
                    { hostname: "mta-prod-2", usage: 2 },
                    { hostname: "mta-prod-3", usage: 0 },
                ]);
                done();
            });
        });

        test("it should return an empty array if _fetch returns nothing", (done) => {
            const service = new IpConfigService();

            jest.spyOn(service, "_fetch");
            mocked(service._fetch).mockResolvedValue([]);

            service.getIpsUsage().then((result) => {
                expect(result).toEqual([]);
                done();
            });
        });

        test("it should throw an error if _fetch throws", (done) => {
            const service = new IpConfigService();

            jest.spyOn(service, "_fetch");
            mocked(service._fetch).mockRejectedValue("Error");

            service.getIpsUsage().catch((error) => {
                expect(error).toEqual("Error");
                done();
            });
        });
    });
});
