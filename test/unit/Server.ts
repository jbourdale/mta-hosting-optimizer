import express from "express";

import Server from "../../src/Server";

describe("Server", () => {
    describe("init", () => {
        it("should return the Server instance", () => {
            const s = new Server().init();

            expect(s instanceof Server).toBeTruthy();
        });

        it("should define and configure app", () => {
            const s = new Server().init();

            expect(s.app).toBeDefined();
        });
    });

    describe("run", () => {
        it("shoud launch the express app", (done) => {
            const s = new Server().init();
            const spy = jest.spyOn(s.app, "listen");

            s.run().then((server) => {
                server.close();
                expect(spy).toBeCalled();
                done();
            });
        });
    });
});
