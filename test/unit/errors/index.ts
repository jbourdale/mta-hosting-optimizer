import {
    HttpError,
    NotFoundHttpError,
    IpConfigServiceError,
} from "../../../src/errors";

describe("errors", () => {
    describe("HttpError", () => {
        it("should accept an status code", () => {
            const error = new HttpError(400);
            expect(error.statusCode).toBe(400);
        });

        it("should accept an optional message", () => {
            const error = new HttpError(400, "message");
            expect(error.message).toBe("message");
        });
    });

    describe("NotFoundHttpError", () => {
        it("should inherit HttpError", () => {
            const error = new NotFoundHttpError();
            expect(error instanceof HttpError).toBeTruthy();
        });

        it("should define 404 status code", () => {
            const error = new NotFoundHttpError();
            expect(error.statusCode).toBe(404);
        });

        it("should define correct message", () => {
            const error = new NotFoundHttpError();
            expect(error.message).toBe("Not Found");
        });
    });

    describe("IpConfigServiceError", () => {
        it("should inherit HttpError", () => {
            const error = new IpConfigServiceError();
            expect(error instanceof HttpError).toBeTruthy();
        });

        it("should define 400 status code", () => {
            const error = new IpConfigServiceError();
            expect(error.statusCode).toBe(400);
        });

        it("should define correct message", () => {
            const error = new IpConfigServiceError();
            expect(error.message).toBe("Unable to retrieve IpConfig data");
        });
    });
});
