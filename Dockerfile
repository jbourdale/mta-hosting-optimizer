FROM node:14-alpine AS builder

WORKDIR /app

COPY package*.json ./
RUN npm ci

COPY tsconfig*.json ./
COPY src src

RUN npm run build

# Runner

FROM node:14-alpine

WORKDIR /app
ENV NODE_ENV=production

COPY --from=builder /app/node_modules .
COPY --from=builder /app/tsconfig* .
COPY --from=builder /app/dist dist/

COPY package*.json ./
RUN npm ci

ENTRYPOINT [ "npm", "run", "serve" ]
