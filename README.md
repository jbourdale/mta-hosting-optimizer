# MTA HOSTING OPTIMIZER

## Introduction

This project has been created to anwser an SRE assignment at Sendinblue.
It must be and remain private.

Author : Jules Bourdalé - jules.bourdale@gmail.com

## General explaination

It defines an Node JS project, using ExpressJS and Typescript.

The `ip-config` folder is a git sub-module for ease the development environment.

## Project Architecture

```
.
├── Dockerfile
├── README.md
├── coverage                # Available after running tests, generate coverage in lcov and html formats
├── dist                    # build directory
├── docker-compose.yml
├── ip-config               # Git Submodule of https://gitlab.com/jbourdale/ip-config
├── jest.config.ts          # Jest (the test framework used in this project) config file
├── node_modules
├── package-lock.json
├── package.json
├── src                     # Source directory
│   ├── index.ts            # App entrypoint
│   ├── Server.ts           # Define the app configuration
│   ├── api
│   │   ├── index.ts
│   │   └── threshold.ts    # /threshold endpoint
│   ├── errors
│   │   └── index.ts        # Define every errors models used in the app
│   ├── index.ts
│   ├── middlewares         # Express middlewares
│   │   ├── errors.ts
│   │   ├── index.ts
│   │   └── routes.ts
│   └── services
│       ├── Configuration.ts  # Centralize the app configuration management
│       ├── IpConfig.ts       # Interact with the IpConfig web service and map it to models
│       └── index.ts
├── test
│   ├── integration
│   │   └── api
│   │       ├── 404.ts
│   │       └── threshold.ts
│   └── unit
│       ├── Server.ts
│       ├── errors
│       │   └── index.ts
│       └── services
│           ├── Configuration.ts
│           └── IpConfig.ts
├── tsconfig.json
└── tslint.json
```

## Configuration

### Environment variables availables :

-   NODE_ENV : Node environment (test | development | production, default: development)
-   LOG_LEVEL : Define the logging level (debug | info | warning | error, default: info)
-   HOST : Define on which interface the server runs (default: 127.0.0.1)
-   PORT : Define on which port the server runs (default: 3000)
-   IP_CONFIG_PROTOCOL : Define which protocol is used to reach the Ip Config service (default: http)
-   IP_CONFIG_HOST : Ip Config Service host (default: ipconfig)
-   IP_CONFIG_PORT : Ip Config Service port (default: 8080)
-   IP_CONFIG_TIMEOUT : Ip Config Service timeout, define the request time acceptance in milliseconds (default: 2000)
-   IP_CONFIG_THRESHOLD : Define the threshold above which an host is used (default: 1)

## Building

Node 14 recommended

```
npm install
npm run build
```

## Testing

```
npm install
npm test # runs unit and integrations tests
npm run test:unit # runs unit tests only
npm run test:integration # run integrations tests only
```

## Linting

```
npm install
npm run lint
```

## Running

### Development

```
npm run dev
```

Will allow the project to be run with hot reload using [ts-node-dev](https://www.npmjs.com/package/ts-node-dev)

### Production build

See [Building](##Building) to build the project first

```
npm run serve
```

## Docker

### Building

```
docker build -t mta-hosting-optimizer .
```

### Running

```
docker run -p 3000:3000 mta-hosting-optimizer
```

### Docker Compose

A docker-compose.yml file is available to run the mta-hosting-optimizer alongs to the ip config services

```
docker-compose up
```
